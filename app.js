const express = require("express");
const app = express();
app.use("/", express.static("build"));
app.listen(8080);

// 在服务器上搭建一个 代理
const { createProxyMiddleware } = require("http-proxy-middleware");
app.use(
  "/api",
  createProxyMiddleware({
    target: "https://www.ahsj.link/rambo",
    changeOrigin: true,
    pathRewrite: {
      "^/api": "",
    },
  })
);
