import { EventBus } from "@/utils/eventBus";
import axios, { AxiosError } from "axios";

const request = axios.create({
  // 如果将来我们打包后，是把打包的结果直接给后端部署上线，我们只需要把这个baseURL 修改为 上线的服务器地址就行
  // baseURL: "https://www.ahsj.link/rambo",
  // 如果我们是写自己的项目来练习的，接口是别人给的
  // 需要放到自己的服务器上 ，需要在自己的服务器上 建立 一个代理， 此时我们的代码还是写的  /api , 然后在我们自己的服务器上，配置对应的代理
  baseURL: "/api",
  timeout: 5000,
});

request.interceptors.request.use(
  (config) => {
    // 判断一下，如果有 token ，就要把 token带回服务器
    let token = localStorage.getItem("token");
    if (token) {
      // 如果已经存在headers，就使用原来的，否则给一个默认的空的对象
      config.headers = config.headers || {};
      config.headers["x-auth-token"] = token;
    }
    EventBus.emit("loading", true);
    return config;
  },
  (err) => Promise.reject(err)
);
request.interceptors.response.use(
  (res) => {
    EventBus.emit("loading", false);
    return res.data;
  },
  (error) => {
    // 如果想让err 有类型提示，只能自己在这做 类型转换
    // console.log(err);
    let err = error as AxiosError<Response<any>>;
    // axios 所有请求， 如果  status Code 不是 200 ， 就一定会进 这个函数
    // 判断，如果 响应回来的数据 的 errCode 是 1002 就是登录失效
    if (err.response?.data.errCode === 1002) {
      // 提示用户
      // 跳转到 login
      // 清空token
      // localStorage.removeItem("token");

      // 我们把上面的逻辑都 解耦 到了订阅发布模式里面，现在只需要发布
      EventBus.emit("logintimeout");
      // 请求无论成功还是失败，都应该把loading隐藏
      EventBus.emit("loading", false);
    }

    return Promise.reject(err);
  }
);

export default request;
