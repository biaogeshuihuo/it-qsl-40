import request from "./request";

export const LoginApi = (params: ILoginParams): RES<string> =>
  request.post("/1024/login", params);

// Home页面的默认数据请求
export const HomeDataApi = (): RES<IHomeDataResonse> => request.get("/6666");

// 根据actionCode 请求 对应的题目的 分类数据
export const getTestTypeAPi = (actionCode: string): RES<IMap[]> =>
  request.get(`/1314/${actionCode}/all`);

// 向服务器请求 练习页面的 题目 数组
export const getQuestionsAPi = (
  params: IQuetionData
): RES<IQuestionResponse[]> => request.post("/1314", params);

// 提交答案
export const submitAnswerApi = (params: IUserAnswer): RES<string> =>
  request.put("/1314", params);

export const getSubjectsApi = (): RES<IexamData[]> =>
  request.get("/6666/field");
