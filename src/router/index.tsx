import App from "@/views/App";
import { lazy } from "react";
// import Fast from "@/views/Fast";
// import Home from "@/views/Home";
import Login from "@/views/Login";
// import Mine from "@/views/Mine";
// import Practise from "@/views/Parctise";
// import Subjects from "@/views/Sbujects";
// import Select from "@/views/Select";
import { HashRouter as Router, Route, Routes } from "react-router-dom";

// react里面实现懒加载的做法：
// const 组件 = lazy(()=>import('路径'))
const Home = lazy(() => import("../views/Home"));
const Subjects = lazy(() => import("../views/Sbujects"));
const Fast = lazy(() => import("../views/Fast"));
const Mine = lazy(() => import("../views/Mine"));
const Select = lazy(() => import("../views/Select"));
const Practise = lazy(() => import("../views/Parctise"));

const router = (
  <Router>
    <Routes>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/" element={<App />}>
        {/* 首页 */}
        <Route path="home" element={<Home />}></Route>
        {/* 切换学科 */}
        <Route path="subjects" element={<Subjects />}></Route>
        {/* 快速刷题目 */}
        <Route path="fast" element={<Fast />}></Route>
        {/* 我的 */}
        <Route path="mine" element={<Mine />}></Route>
        {/* 选题 */}
        <Route path="select/:actionCode" element={<Select />}></Route>
        {/* 练习 */}
        <Route path="parictise" element={<Practise />}></Route>
      </Route>
    </Routes>
  </Router>
);

export default router;
