import { EventBus } from "@/utils/eventBus";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import LoadingIcon from "../LodingIcon";
import "./mask.less";

export default function Mask() {
  const isShow = useSelector((state: IRootState) => state.loading.isShow);

  //   因为这是一个全局的组件，无论在哪个页面，只要进入了这个网站，就会有当前的mask组件
  // 所以可以直接在这里面订阅 loading 效果
  const dispatch = useDispatch();
  useEffect(() => {
    EventBus.on("loading", (payload: boolean) => {
      dispatch({ type: "LOADING", payload });
    });
  }, []);

  return (
    <div className="mask" style={{ display: isShow ? "block" : "none" }}>
      <LoadingIcon />
    </div>
  );
}
