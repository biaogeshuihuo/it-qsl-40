import React from "react";
import "./lodingIcon.less";

export default function LoadingIcon() {
  return (
    <div className="loding-icon">
      <div className="icon"></div>
    </div>
  );
}
