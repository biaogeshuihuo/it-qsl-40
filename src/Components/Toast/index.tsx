/**
 *  Toast 提示框是 一个 公共组件，在多个页面都要用到 --- 单独封装
 *
 *      封装组件的思路：
 *          1. 考虑需要什么功能
 *                 需要提示用户一些信息的的时候显示出来
 *
 *          2. 由功能推断出，这个组件需要向外暴露什么属性
 *                  a.提示信息
 *                  b.提示的类型
 *                  c.是否显示出来
 *
 *          3. 实现功能
 */

import { Alert, AlertColor } from "@mui/material";
import React from "react";
import { useDispatch, useSelector } from "react-redux";

// interface IProps {
//   type: AlertColor; // severity 属性只能是 4个值，所以人家定义了一个对应的类型来实现
//   message: string;
// }

let timer: NodeJS.Timeout; // 使用这个类型告诉后面拉手的倒霉蛋这是一个 定时器

export default function Toast() {
  // type和message 已经存到了 redux，所以直接从redux获取就行
  const { type, message, isShow } = useSelector(
    (state: IRootState) => state.toastData
  );

  // 希望 toat 组件 能自动消失 -- 定时器 修改 isShow 为false
  const dispatch = useDispatch();
  // 为了让每点提示一定是展示同样长的时间，就要让上一个定时器 停下
  clearTimeout(timer);
  // 我们需要在 isShow 是true的时候才做防抖
  if (isShow) {
    timer = setTimeout(() => {
      dispatch({
        type: "HIDE_TOAST",
      });
    }, 2000);
  }

  return (
    <div
      style={{
        display: isShow ? "block" : "none",
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
      }}
    >
      <Alert severity={type}>{message}</Alert>
    </div>
  );
}
