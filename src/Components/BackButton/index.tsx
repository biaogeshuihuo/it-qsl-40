import { Button } from "@mui/material";
import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./backbutton.less";

// 有部分路由里面是不显示这个按钮
const routes = ["/home", "/fast", "/mine"];

export default function BackButton() {
  const location = useLocation();
  const navigate = useNavigate();
  return (
    <div
      className="back"
      style={{ display: routes.includes(location.pathname) ? "none" : "block" }}
    >
      <Button fullWidth variant="contained" onClick={() => navigate(-1)}>
        返回上一页
      </Button>
    </div>
  );
}
