import { BottomNavigation, BottomNavigationAction } from "@mui/material";
import React, { useEffect } from "react";
import "./bottomnav.less";

// 导入图片
import fastIcon from "@/assets/images/tabbar/fast.png";
import homeActiveIcon from "@/assets/images/tabbar/home_1.png";
import homeIcon from "@/assets/images/tabbar/home_2.png";
import mineActiveIcon from "@/assets/images/tabbar/my_1.png";
import mineIcon from "@/assets/images/tabbar/my_2.png";
import { useLocation, useNavigate } from "react-router-dom";

// 如果要根据不同的登录的用户，决定下面底部导航的内容，数据肯定是从服务器获取的，但是现在没有这个接口，所以我们先不写成在 组件内 动态请求的
// 先写成一个固定的，将来如果要写成从服务器获取，就再修改一个就好
const nav = [
  {
    label: "首页",
    icon: {
      active: homeActiveIcon,
      normal: homeIcon,
    },
  },
  {
    label: "快速刷题",
    icon: {
      active: fastIcon,
      normal: fastIcon,
    },
  },
  {
    label: "我的",
    icon: {
      active: mineActiveIcon,
      normal: mineIcon,
    },
  },
];

// 把要跳转的路由 使用一个数组 存起来
const routes = ["/home", "/fast", "/mine"];

export default function BottomNav() {
  const [value, setValue] = React.useState(0);
  const navigate = useNavigate();

  //   监听路由变化，修改 value
  const location = useLocation();
  useEffect(() => {
    // 修改value
    const idx = routes.indexOf(location.pathname);
    // if (idx >= 0) {
    setValue(idx);
    // }
  }, [location.pathname]);

  return (
    <div
      className="bottom-nav"
      style={{ display: routes[value] ? "block" : "none" }}
    >
      <BottomNavigation
        showLabels
        value={value}
        onChange={(event, newValue) => {
          //   跳转到不同页面
          navigate(routes[newValue]);
          setValue(newValue);
        }}
      >
        {nav.map((item, index) => (
          <BottomNavigationAction
            key={item.label}
            label={item.label}
            icon={
              <img
                src={value === index ? item.icon.active : item.icon.normal}
              />
            }
          />
        ))}
        {/* <BottomNavigationAction
          label="快速刷题"
          icon={<img src={fastIcon} />}
        />
        <BottomNavigationAction
          label="我的"
          icon={<img src={value === 2 ? mineActiveIcon : mineIcon} />}
        /> */}
      </BottomNavigation>
    </div>
  );
}
