import { LoginApi } from "@/api";
import React, { useState } from "react";
import { Button, TextField } from "@mui/material";
// 在webpack里面导入图片有两个方式
// import   ---  立刻导入，在打包的时候就会把这个图片一起打包到 首次加载的 js 里面<把图片转换为base64编码>
// require  ---  延迟导入，会在用到的时候才加载，打包的时候不会打包到首次加载的js里面
import logo from "@/assets/images/logo.png";
import "./Login.less";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import useToast from "@/utils/cusHook";
export default function Login() {
  // 定义一个变量，控制 当前的页面 是用于注册还是用于登录
  const [isLoginPage, setIsLoginPage] = useState(true);
  // 定义用户名和密码的变量
  const [userName, setUserName] = useState("12345678901");
  const [password, setPassword] = useState("wolfcode123");
  // 可以跳转的 navigate 方法
  const navigate = useNavigate();

  // const dispatch = useDispatch();
  // 调用 useToast 得到一个函数，函数里面 再 dispatch 逻辑
  const toast = useToast();

  const login = () => {
    // 请求应该是 在 一段时间内只能做一次，所以需要做一个 节流 (自己实现)
    LoginApi({
      username: userName,
      password,
    })
      .then((res) => {
        if (res.errCode === 0) {
          // 登录成功
          // alert("登录成功");
          toast("success", "登录成功");
          // 存token
          localStorage.setItem("token", res.data);
          // 跳转到 home
          navigate("/home");
        }
        // 此时失败的情况是 我们从响应拦截里面处理，因为这里后端在处理不正确的时候，返回的不是 errCode 不是0
        // 而是 status 已经是 500
      })
      .catch((err) => {
        // 也可以在 catch 里面处理，但是我们如果能在一个统一处理的地方实现，最好
        console.log(err);
      });
  };

  const register = () => {};

  const clickHanle = () => {
    // 获取 用户名 和 密码
    // 判断非空 - 提示
    if (userName.trim() === "") {
      // dispatch({
      //   type: "SHOW_TOAST",
      //   payload: {
      //     type: "error",
      //     message: "用户名不能为空",
      //   },
      // });
      toast("error", "用户名不能为空");
      return;
    }
    if (!/^[1]\d{10}$/.test(userName)) {
      return toast("error", "手机号格式不对");
    }
    if (password.trim() === "") {
      return toast("error", "密码不能为空");
    }
    // 判断当前是登录还是注册
    isLoginPage ? login() : register();
  };

  return (
    <div className="login">
      <div className="logo">
        <img src={logo} alt="" />
      </div>
      <form>
        <div className="row">
          <TextField
            fullWidth
            placeholder="请输入手机号"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="row">
          <TextField
            fullWidth
            type="password"
            placeholder="请输入密码"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="row">
          <Button variant="contained" fullWidth onClick={clickHanle}>
            {isLoginPage ? "直接登录" : "立刻注册"}
          </Button>
        </div>
        <div className="row">
          <Button onClick={() => setIsLoginPage(!isLoginPage)}>
            {isLoginPage ? "前往注册" : "返回登录"}
          </Button>
        </div>
      </form>
    </div>
  );
}
