import { getTestTypeAPi } from "@/api";
import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import SelectList from "./components/SelectList";
import "./select.less";
import { qsTypeMap } from "@/utils/common";

// 试题分类的数组
const testTypes: IMap[] = [
  { key: "全部", value: "all" },
  { key: "未做", value: "notdone" },
  { key: "已做", value: "done" },
  { key: "错误", value: "err" },
];

// 做题数量的数组
const testNums: IMap[] = [
  { key: "5", value: "5" },
  { key: "10", value: "10" },
  { key: "20", value: "20" },
  { key: "30", value: "30" },
  { key: "50", value: "50" },
  { key: "100", value: "100" },
];

export default function Select() {
  // 题目数量
  const [testNum, setTestNum] = useState(5);
  // 试题分类
  const [testType, setTestType] = useState("all");
  // 题目分类
  const [questionType, setQuestionType] = useState("all");
  //  通过点击 试题分类，获取 对应的分类
  const getTestType = (val: string) => {
    // 希望val 应该是 err , all , done , notdone
    // console.log(val);
    setTestType(val);
  };

  // 获取题目数量
  const getTestNum = (val: string) => {
    // console.log(val);
    setTestNum(parseInt(val));
  };

  // 获取题目分类
  const getQuestionType = (val: string) => {
    // console.log(val);
    setQuestionType(val);
  };

  // 从路由里面获取到acionCode
  const { actionCode } = useParams<{ actionCode: string }>();
  // 定义一个 用来展示中间的题目分类的数据
  const [questionTypes, setQuestionTypes] = useState<IMap[]>([]);

  useEffect(() => {
    // 因为 actionCode 可能没有，所以需要先判断一下，再请求，ts才认为是安全
    // if(actionCode){
    // getTestTypeAPi(actionCode);
    // }

    // 在一个可以为空的数据后面，加上一个 ! ---> 类型断言， 告诉ts，这个数据不是你认为的那样，而一定有
    getTestTypeAPi(actionCode!).then((res) => {
      // 根据actionCode已经所它对应的题目的类型获取回来了
      if (res.errCode === 0) {
        // 需要把 res.data 转换为 我们想要的数据 ==> {key: 全部24 , value:'all' , key: 问答12 ,value: qa ...}
        // 也就是说 原本：  {key: all , value : 24}  ====> 最终： {key: 全部24 , value:'all'}
        const temp: IMap[] = res.data.map((item) => {
          const key = qsTypeMap[item.key] + item.value;
          const value = item.key;
          return { key, value };
        });

        setQuestionTypes(temp);
      }
    });
  }, []);

  const navigate = useNavigate();
  const toPractice = () => {
    // 跳转到 练习页面，要带上 4个数据
    navigate("/parictise", {
      state: {
        testNum,
        testType,
        actionCode,
        questionType,
      },
    });
  };

  return (
    <div className="select">
      <div className="flex">
        <Button variant="contained" color="info" onClick={toPractice}>
          进入练习模式
        </Button>
        <Button variant="contained" color="warning">
          进入考试模式
        </Button>
      </div>

      <SelectList title="试题分类" list={testTypes} onChange={getTestType} />
      {/* 中间题目的分类 */}
      <SelectList
        title="题目分类"
        list={questionTypes}
        onChange={getQuestionType}
        slice={3}
      />

      <SelectList title="做题数量" list={testNums} onChange={getTestNum} />
    </div>
  );
}
