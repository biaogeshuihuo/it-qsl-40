/**
 *  在选择题目的页面，重复使用的 点击选择不同数据的组件
 *
 */
import React, { useState } from "react";
import "./selectlist.less";

// 分析得到，要从外部传入的数据
// title -- 标题
// list - 选项的数组 ， 对象数组， 对象就要有两个属性： key - 显示给用户看到有内容 ，value - 选中得到的内容
interface Iprop {
  title: string;
  list: IMap[];
  onChange: (val: string) => void;
  // 如果是占2份，传2，如果占3份，传3
  slice?: number;
}

export default function SelectList({
  title,
  list,
  onChange,
  slice = 2,
}: Iprop) {
  const [idx, setIdx] = useState(0);

  const changeFn = (index: number, value: string) => {
    setIdx(index);
    onChange(value);
  };

  const col = "col-" + slice;

  return (
    <div className="select-list">
      <h3>{title}</h3>
      <ul>
        {list.map((item, index) => (
          <li
            key={item.key}
            className={[col, idx === index ? "active" : ""].join(" ")}
            onClick={() => changeFn(index, item.value)}
          >
            {item.key}
          </li>
        ))}
      </ul>
    </div>
  );
}
