import BackButton from "@/Components/BackButton";
import BottomNav from "@/Components/BottomNav";
import useToast from "@/utils/cusHook";
import { EventBus } from "@/utils/eventBus";
import React, { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

export default function App() {
  const navigate = useNavigate();
  const toast = useToast();
  // 在 所有的 需要登录后才能访问的 路由 的 父路由 对应的 组件 里面实现 路由拦截
  // 在访问所有需要登录的页面的时候，都会经过这，所以我们只需要在这做判断
  // 判断token是否存在
  let token = localStorage.getItem("token");
  useEffect(() => {
    if (!token) {
      // 跳转回login
      navigate("/login");
    }

    // 在App 里面实现 在响应拦截里面需要的 弹框和中转到登录
    EventBus.on("logintimeout", () => {
      // 如果登录超时了，就应该提示用户
      toast("error", "登录失效，请求重新登录");
      // 跳转到login
      navigate("/login");
      // 清空token
      localStorage.removeItem("token");
    });
  }, []);

  // 然后也要判断， 如果没有token ，就不返回后续的渲染
  return token ? (
    <>
      <Outlet />
      <BottomNav />
      <BackButton />
    </>
  ) : (
    <></>
  );
}
