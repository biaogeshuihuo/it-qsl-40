import { Button, LinearProgress } from "@mui/material";
import React from "react";
import "./subject.less";
const img = require("@/assets/images/home/subject.png");

interface IProp {
  data: IexamData;
  onClick: () => void;
}

export default function SubjectsItem({ data, onClick }: IProp) {
  return (
    <div className="subject-item flex">
      <img src={img} alt="" />
      <section className="mid">
        <h3>{data.title}</h3>
        <p>
          {data.itemCount}/{data.itemCount}题
        </p>
        <LinearProgress variant="determinate" value={50} />
      </section>
      <Button onClick={onClick} variant="contained">
        练习
      </Button>
    </div>
  );
}
