import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { ArrowRight } from "@mui/icons-material";
import "./home.less";
import { HomeDataApi } from "@/api";
import SubjectsItem from "./components/SubjectsItem";
import { useNavigate } from "react-router-dom";

export default function Home() {
  // 收藏数
  const [collect, setCollect] = useState(0);
  const [study, setStudy] = useState(0);
  const [wrong, setWrong] = useState(0);
  const [total, setTotal] = useState(0);
  const [title, setTitle] = useState("");
  // 声明一个 学科列表的数组
  const [list, setList] = useState<IexamData[]>([]);
  // 一进入页面，就要发请求
  useEffect(() => {
    HomeDataApi().then((res) => {
      if (res.errCode === 0) {
        setCollect(res.data.collect);
        setStudy(res.data.study);
        setWrong(res.data.wrong);
        setTotal(res.data.exam.itemCount);
        setTitle(res.data.exam.title);
        // 更新 数组 数据
        setList(res.data.exemItems);
      }
    });
  }, []);

  const navigate = useNavigate();
  // 是练习按钮点击的事件
  const clickHandle = (code: string) => {
    // 点击之后要跳转到 选择题目的页面
    navigate("/select/" + code);
  };

  return (
    <div className="home">
      <section className="subjects flex between">
        <h1>{title}</h1>
        <Button variant="text">
          <span>切换考试科目</span>
          <ArrowRight />
        </Button>
      </section>
      <section className="welcom">
        <p>欢迎</p>
        <section className="flex">
          <section className="img"></section>
          <section>
            <div className="flex">
              <span>已学{study}题</span> | <span>共{total}题</span>
            </div>
            <div className="flex">
              <div className="flex">
                <span className="count">{wrong}</span>
                <span className="text">错题</span>
              </div>
              <div className="flex">
                <span className="count">{collect}</span>
                <span className="text">收藏</span>
              </div>
            </div>
          </section>
        </section>
      </section>
      {/* 学科列表 */}
      <section className="subject-list">
        {list.map((item) => (
          <SubjectsItem
            key={item.id}
            data={item}
            onClick={() => clickHandle(item.actionCode)}
          />
        ))}
      </section>
    </div>
  );
}
