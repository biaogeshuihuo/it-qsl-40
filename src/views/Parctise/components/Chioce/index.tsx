// 专门显示选择类型的题目
import React, { useState } from "react";
import ChioceItem from "./ChioceItem";
import "./chiose.less";
import { qsTypeMap } from "@/utils/common";
import { Button } from "@mui/material";
import useToast from "@/utils/cusHook";
import { submitAnswerApi } from "@/api";

// 需要从上层组件获取到每个题目组件的 数据
interface Iprop {
  data: IQuestionResponse;
  actionCode: string;
}

export default function Chioce({ data, actionCode }: Iprop) {
  const [aswers] = useState<IMap[]>(JSON.parse(data.optionContent));

  // 在这保存一个数组，里面是哪些选项被勾选 , 也就是 A B C D
  const [userAnsers, setUserAnsers] = useState<string[]>([]);

  // 让子组件通知父组件进行选择
  const changeAnswer = (val: string) => {
    // console.log(val);
    // 如果是多选
    if (data.questionType === "many") {
      let idx = userAnsers.indexOf(val);
      if (idx == -1) {
        // 如果 数组里面没有 对应 答案， 就push进行
        setUserAnsers([...userAnsers, val]);
      } else {
        // 如果已经存在答案数组里面， 就从里面删除
        // userAnsers.splice(idx, 1);
        // setUserAnsers([...userAnsers]);
        setUserAnsers((arr) => {
          arr.splice(idx, 1);
          return [...arr];
        });
      }
    } else {
      // 如果是 单选 和 判断 ， 直接把 数组里面的数据 修改成我们传回的数据就行
      setUserAnsers([val]);
    }
  };

  const toast = useToast();
  // 点击确定，提示正确与否
  const btnClick = () => {
    // 判断用户选了没有
    if (userAnsers.length === 0) {
      return toast("error", "请先选择答案");
    }
    // 判断 用户选择的答案是否正确
    const answer = userAnsers.sort().join(",");
    let isRight = answer === data.answer;
    isRight ? toast("success", "回答正确") : toast("error", "回答错误");

    // 把答案提交给服务器
    let params = {
      actionType: "exam_test",
      categoryCode: data.categoryCode,
      userAnswer: answer,
      actionCode,
      id: data.id,
    };

    submitAnswerApi(params).then((res) => {
      // if(res.errCode === 0){
      // }
    });

    // 显示参考答案
  };

  return (
    <div className="chioce">
      <div className="title">
        <div className="cate">{qsTypeMap[data.questionType]}</div>
        <div className="content">{data.title}</div>
      </div>
      {/* 选择的答案 */}
      <div className="answers">
        {aswers.map((item) => (
          <ChioceItem
            data={item}
            key={item.key}
            checked={userAnsers}
            onChange={changeAnswer}
          />
        ))}
      </div>
      <div>
        <Button variant="outlined" onClick={btnClick}>
          确定
        </Button>
      </div>
    </div>
  );
}
