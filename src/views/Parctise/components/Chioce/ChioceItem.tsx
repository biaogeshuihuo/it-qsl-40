import React from "react";
import "./chioseitem.less";
const check = require("@/assets/images/user/choice_me.png");
// 每个选项的内容，也是从外部传入
interface Iprop {
  data: IMap;
  checked: string[];
  onChange: (val: string) => void;
}

export default function ChioceItem({ data, checked, onChange }: Iprop) {
  return (
    <div className="chioce-item" onClick={() => onChange(data.key)}>
      <div className="checked">
        <img
          src={check}
          alt=""
          style={{ display: checked.includes(data.key) ? "block" : "none" }}
        />
      </div>
      <div className="key">{data.key}</div>
      <div className="value">{data.value}</div>
    </div>
  );
}
