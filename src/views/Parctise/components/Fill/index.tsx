// 专门实现 填空或者是问答的组件

import React from "react";
import { qsTypeMap } from "@/utils/common";
import "./fill.less";
import { TextField } from "@mui/material";

interface Iprop {
  data: IQuestionResponse;
  actionCode: string;
}

export default function Fill({ data, actionCode }: Iprop) {
  return (
    <div className="fill">
      <div className="title">
        <div className="cate">{qsTypeMap[data.questionType]}</div>
        <div className="content">{data.title}</div>
      </div>
      <div
        className="code"
        dangerouslySetInnerHTML={{ __html: data.content }}
      ></div>
      <TextField multiline rows={8} fullWidth />
    </div>
  );
}
