import React, { useEffect, useState } from "react";
import { Drawer } from "@mui/material";
import { getSubjectsApi } from "@/api";

export default function Subjects() {
  const closeFn = () => {};

  const [list, setList] = useState<IexamData[]>([]);

  useEffect(() => {
    // 优先从 sessionStorage 里面获取数据
    let data = sessionStorage.getItem("subject-list");
    if (data) {
      // 把从sessionStorage 里面获取到的 字符串， 转换为 数组对象
      let temp = JSON.parse(data) as IexamData[];
      setList(temp);
    } else {
      // 当 sessionStorage 里面没有数据的时候，我们才 需要向 服务器发请求获取数据
      getSubjectsApi().then((res) => {
        if (res.errCode === 0) {
          setList(res.data);
          // 数据回来了， 就需要先 缓存 起来
          sessionStorage.setItem("subject-list", JSON.stringify(res.data));
        }
      });
    }
  }, []);

  return (
    <div>
      <button>切换学科</button>
      <Drawer anchor="left" open={true} onClose={closeFn}>
        <ul>
          {list.map((item) => (
            <li key={item.id}>{item.title}</li>
          ))}
        </ul>
      </Drawer>
    </div>
  );
}
