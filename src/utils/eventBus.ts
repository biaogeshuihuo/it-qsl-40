interface IEvents {
  [index: string]: Function[];
}

interface IEventBus {
  events: IEvents;
  on: (type: string, fn: Function) => void;
  emit: (type: string, payload?: any) => void;
}

// 这是一个负责发布订阅的对象
export const EventBus: IEventBus = {
  events: {},
  // 订阅方法
  on(type: string, fn: Function) {
    // 在 on 方法中， 负责把一个字符串和多个要调用的函数绑定
    if (!this.events[type]) {
      this.events[type] = [];
    }
    this.events[type].push(fn);
  },
  // 发布
  emit(type: string, payload?: any) {
    // 在 emit 方法里面 负责 根据一个字符串，得到所有用来订阅的函数，然后调用这些函数
    if (this.events[type]) {
      this.events[type].forEach((fn) => fn(payload));
    }
  },
};
