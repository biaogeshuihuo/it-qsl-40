// 准备一个 题目类型 的英文到中文的 映射
export const qsTypeMap: StringIndexMap = {
  all: "全部",
  qa: "问答",
  one: "单选",
  check: "判断",
  code: "编程",
  many: "多选",
};
