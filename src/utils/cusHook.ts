import { useDispatch } from "react-redux";
import { Dispatch } from "redux";

function useToast() {
  const dispatch = useDispatch<Dispatch<IToastAction>>();
  return (type: AlertColor, message: string) => {
    dispatch({
      type: "SHOW_TOAST",
      payload: {
        type,
        message,
      },
    });
  };
}

export default useToast;
