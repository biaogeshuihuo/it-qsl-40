import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import "@/assets/styles/common.less";
import router from "./router";
import store from "./store";
import { Provider } from "react-redux";
import Toast from "./Components/Toast";
import Mask from "./Components/Mask";
import LoadingIcon from "./Components/LodingIcon";
// import cssReset from "cssreset";
// cssReset();

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
    <Toast />
    <Mask />
    {/* {router} */}
    {/* 配合懒加载，最好有一个延迟的提示 */}
    <Suspense fallback={<LoadingIcon />}>{router}</Suspense>
  </Provider>
);
