// 用于请求 登录的 参数对象
interface ILoginParams {
  password: string;
  username: string;
}

interface Response<T> {
  errCode: number;
  message: string;
  data: T;
}

// 我们发现 所有的 请求 响应回来的数据 的泛型 约束，前半段是一样的
// Promise<Response<string>>
// Promise<Response<number>>
// Promise<Response<boolean>>
// 再把前半段复用起来
type RES<T> = Promise<Response<T>>;

// 首页数据结构
interface IHomeDataResonse {
  collect: number;
  exam: IexamData;
  exemItems: IexamData[];
  study: number;
  wrong: number;
}

interface IexamData {
  actionCode: string;
  actionName: string;
  actionType: string;
  businessLevel: string;
  createTime: string;
  createUser: string;
  id: number;
  info: string;
  invalid: number;
  itemCount: number;
  numberLevel: number;
  pid: number;
  sort: number;
  tenantId: number;
  title: string;
  updateTime: string;
}

interface IQuetionData {
  testNum: number;
  testType: string;
  actionCode: string;
  questionType: string;
}

interface IQuestionResponse {
  analysis: string;
  answer: string;
  categoryCode: string;
  collcetStatus: false;
  collcetTotal: number;
  content: string;
  id: number;
  invalid: number;
  oerationTotal: number;
  optionContent: string;
  questionType: string;
  score: number;
  sort: number;
  tenantId: number;
  title: string;
  userAnswer: string;
  wrongAnswer: number;
}

interface IUserAnswer {
  categoryCode: string;
  actionType: string;
  userAnswer: string;
  id: number;
  actionCode: string;
}
