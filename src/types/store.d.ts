/**
 *   这种  xxx.d.ts  叫全局声明文件 ，这里面一般就是用来声明对应的一些类型
 *
 *
 */

type AlertColor = "success" | "info" | "warning" | "error";

interface IRootState {
  // 专门用于让全局的toast组件存储对应的数据的
  toastData: IToastData;
  loading: ILoadingData;
}

interface IAction {
  type: string;
  payload: any;
}

interface IToastAction extends IAction {
  payload: IToastData;
}

interface IToastData {
  type: AlertColor;
  message: string;
  isShow?: boolean;
}

interface ILoadingData {
  isShow: boolean;
}
