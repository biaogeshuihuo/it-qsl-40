interface IMap {
  key: string;
  value: string;
  sort?: number;
}

interface StringIndexMap {
  [index: string]: string; // 在ts里面严格规定，要添加了索引器的对象，才能使用 [key] 获取值
}
