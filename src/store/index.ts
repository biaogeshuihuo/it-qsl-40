/*
    1. 创建一个 store ， 并导出
    2. 在index.tsx 入口文件里面 使用Provider 进行数据的共享
*/

import { createStore } from "redux";

const toastData: IToastData = {
  type: "success",
  message: "ok",
  isShow: false,
};

const loading: ILoadingData = {
  isShow: false,
};

const defValue: IRootState = {
  toastData,
  loading,
};

const reduder = (state = defValue, action: IAction) => {
  state = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case "SHOW_TOAST":
      // 修改 type和message
      const { type, message } = action.payload as IToastData; // 将 payload 转换为对应的类型
      state.toastData.type = type;
      state.toastData.message = message;
      // 修改 isShow 为true
      state.toastData.isShow = true;
      break;
    case "HIDE_TOAST":
      state.toastData.isShow = false;
      break;
    case "LOADING":
      state.loading.isShow = action.payload as boolean;
      break;
  }

  return state;
};

const store = createStore(reduder);

export default store;
